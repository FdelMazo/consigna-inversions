#language: es
Característica: Cartera de inversiones de empresa

  Antecedentes:
    Dado que mi cuit es "30445556667"
    
@wip
  Esquema del escenario: Inversion solo en dolares
    Dado que compro $ <capital> dólares a cotizacion $ <cotizacion_que_compre>
    Cuando vendo esos dolares a cotización $ <cotizacion_que_vendi>
    Entonces obtengo una ganancia bruta de $ <ganancia_bruta>
    Y $ <impuestos_comisiones> de impuestos-comisiones
    Y eso resulta en una ganancia neta de $ <ganancia_neta>

  Ejemplos:
    | capital | cotizacion_que_compre | cotizacion_que_vendi | ganancia_bruta | impuestos_comisiones |ganancia_neta |
    |    1000 |        80.5           |      90.0            |    9500.0      |         95.0         |    9405.0    |
    |    50000|        80.5           |      90.0            |    475000.0    |     171000.0         |    304000    |
    |    1000 |        80.5           |      101.0           |    20500.0     |       1230.0         |    19270.0   |
    |    1000 |        80.5           |      125.5           |    45000.0     |       7200.0         |    37800.0   |

  @wip
  Esquema del escenario: Inversion solo en plazo fijo
    Dado que el interes de plazo fijo es <interes> % anual
    Cuando invierto $ <capital> en un plazo fijo a <plazo> dias
    Entonces obtengo una ganancia bruta de $ <ganancia_bruta>
    Y $ <impuestos_comisiones> de impuestos-comisiones
    Y eso resulta en una ganancia neta de $ <ganancia_neta>

Ejemplos:
    | interes | capital | plazo | ganancia_bruta | impuestos_comisiones |ganancia_neta |
    |   10    |   1000  | 365   |     100.0      |         0.0          |     100.0    |
    |   21    |  100000 | 365   |     21000      |         1050.0       |     19950.0  |
    |   40    |  100000 | 365   |     40000      |         6000.0       |     34000.0  |
    |   80    |  100000 | 365   |     80000      |         28000.0      |     52000.0  |
    |   80    |  1000   | 73    |     160        |         0.0          |     160.0    |

    @wip
  Escenario: Inversion solo en plazo fijo con plazo excedido
    Dado que el interes de plazo fijo es 10 % anual
    Cuando invierto $ 1000 en un plazo fijo a 500 dias
    Entonces obtengo un error "Plazo no valido"

  @wip
  Escenario: Inversion solo en acciones
    Dado que compro $ 1000.0 en acciones a $ 50.0 por acción
    Cuando vendo esas acciones a $ 100.0 por acción
    Entonces obtengo una ganancia bruta de $ 1000.0
    Y $ 10.0 de impuestos-comisiones
    Y eso resulta en una ganancia neta de $ 990.0

