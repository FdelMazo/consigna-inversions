#language: es
Característica: Cartera personas

  Antecedentes:
    Dado que mi cuit es "20112223336"

  @wip
  Escenario: Inversiones varias en dolares
    Dado que compro $ 1000 dólares a cotizacion $ 80.5
    Cuando vendo esos dolares a cotización $ 90
    Y que compro $ 10000 dólares a cotizacion $ 100.0
    Cuando vendo esos dolares a cotización $ 200.0
    Entonces obtengo una ganancia bruta de $ 1009500.0
    Y $ 40380.0 de impuestos-comisiones
    Y eso resulta en una ganancia neta de $ 969120.0

  @wip
  Escenario: Inversion en plazo fijo y acciones
    Dado que el interes de plazo fijo es 10 % anual
    Y invierto $ 1000.0 en un plazo fijo a 365 dias
    Y que compro $ 1000.0 en acciones a $ 50.0 por acción
    Y vendo esas acciones a $ 100.0 por acción
    Entonces obtengo una ganancia bruta de $ 1100.0
    Y $ 10.0 de impuestos-comisiones
    Y eso resulta en una ganancia neta de $ 1090.0

  @wip
  Escenario: Un caso que tenga los 3 tipos de inversiones
    Dado que compro $ 10 dólares a cotizacion $ 1
    Y que el interes del plazo fijo es de 10 % anual
    Y que compro $ 1000.0 en acciones a $ 50.0 por acción

    Cuando invierto invierto $ 1000  en un plazo fijo a 365 dias 
    Y vendo esos dolares a cotización $ 3 
    Y vendo esas acciones a $ 100.0 por acción

    Entonces obtengo una ganancia bruta de  1120
    Y eso resulta en una ganancia neta de 1109.8
    Y 10.2 de impuestos-comisiones 
    
